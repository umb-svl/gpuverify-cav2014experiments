AMD_SDK/AtomicCounters/kernel2/kernel.cl
AMD_SDK/BinarySearch/kernel1/kernel.cl
AMD_SDK/BinarySearch/kernel2/kernel.cl
AMD_SDK/BinomialOption/kernel.cl
AMD_SDK/BinomialOptionMultiGPU/kernel.cl
AMD_SDK/BitonicSort/kernel.cl
AMD_SDK/BlackScholes/kernel.cl
AMD_SDK/BlackScholesDP/kernel.cl
AMD_SDK/BoxFilter/kernel1/kernel.cl
AMD_SDK/BoxFilter/kernel2/kernel.cl
AMD_SDK/BoxFilter/kernel3/kernel.cl
AMD_SDK/BoxFilter/kernel4/kernel.cl
AMD_SDK/BoxFilter/kernel5/kernel.cl
AMD_SDK/BoxFilter/kernel6/kernel.cl
AMD_SDK/BoxFilter/kernel7/kernel.cl
AMD_SDK/BoxFilterGL/kernel1/kernel.cl
AMD_SDK/BoxFilterGL/kernel2/kernel.cl
AMD_SDK/BoxFilterGL/kernel3/kernel.cl
AMD_SDK/BoxFilterGL/kernel4/kernel.cl
AMD_SDK/BoxFilterGL/kernel5/kernel.cl
AMD_SDK/BoxFilterGL/kernel6/kernel.cl
AMD_SDK/BoxFilterGL/kernel7/kernel.cl
AMD_SDK/BufferBandwidth/kernel1/kernel.cl
AMD_SDK/BufferBandwidth/kernel2/kernel.cl
AMD_SDK/DCT/kernel.cl
AMD_SDK/DeviceFission/kernel.cl
AMD_SDK/DwtHaar1D/kernel.cl
AMD_SDK/EigenValue/kernel1/kernel.cl
AMD_SDK/EigenValue/kernel2/kernel.cl
AMD_SDK/FastWalshTransform/kernel.cl
AMD_SDK/FloydWarshall/kernel.cl
AMD_SDK/FluidSimulation2D/kernel.cl
AMD_SDK/Histogram/kernel.cl
AMD_SDK/HistogramAtomics/kernel1/kernel.cl
AMD_SDK/HistogramAtomics/kernel2/kernel.cl
AMD_SDK/ImageBandwidth/kernel1/kernel.cl
AMD_SDK/ImageBandwidth/kernel2/kernel.cl
AMD_SDK/KernelLaunch/kernel1/kernel.cl
AMD_SDK/KernelLaunch/kernel2/kernel.cl
AMD_SDK/LUDecomposition/kernel2/kernel.cl
AMD_SDK/MatrixMulImage/kernel3/kernel.cl
AMD_SDK/MatrixMultiplication/kernel1/kernel.cl
AMD_SDK/MatrixMultiplication/kernel2/kernel.cl
AMD_SDK/MatrixTranspose/kernel.cl
AMD_SDK/MersenneTwister/kernel.cl
AMD_SDK/MonteCarloAsian/kernel.cl
AMD_SDK/MonteCarloAsianMultiGPU/kernel.cl
AMD_SDK/NBody/kernel.cl
AMD_SDK/PrefixSum/kernel.cl
AMD_SDK/QuasiRandomSequence/kernel.cl
AMD_SDK/RecursiveGaussian/kernel1/kernel.cl
AMD_SDK/RecursiveGaussian/kernel2/kernel.cl
AMD_SDK/Reduction/kernel.cl
AMD_SDK/ScanLargeArrays/kernel1/kernel.cl
AMD_SDK/ScanLargeArrays/kernel2/kernel.cl
AMD_SDK/ScanLargeArrays/kernel3/kernel.cl
AMD_SDK/SimpleConvolution/kernel.cl
AMD_SDK/SimpleGL/kernel.cl
AMD_SDK/SimpleImage/kernel1/kernel.cl
AMD_SDK/SimpleImage/kernel2/kernel.cl
AMD_SDK/SimpleMultiDevice/kernel.cl
AMD_SDK/SobelFilter/kernel.cl
AMD_SDK/Template/kernel.cl
AMD_SDK/TemplateC/kernel.cl
AMD_SDK/TransferOverlap/kernel1/kernel.cl
AMD_SDK/URNG/kernel.cl
AMD_SDK/URNGNoiseGL/kernel.cl
C++AMP/3000.imgblur_grid_6662C876/imgblur_Beta/kernel.cu
C++AMP/3000.imgblur_grid_6662C876/imgblur_grid/kernel.cu
C++AMP/BinomialOptions/kernel.cu
C++AMP/BitonicSort/bitonic_sort_kernel/kernel.cu
C++AMP/BitonicSort/transpose_kernel/kernel.cu
C++AMP/Convolution/convolution_simple/kernel.cu
C++AMP/Convolution/convolution_tiling/kernel.cu
C++AMP/DxInterOp/kernel.cu
C++AMP/HelloWorldCSharp/kernel.cu
C++AMP/Histogram/histo_merge_kernel/kernel.cu
C++AMP/MatrixMultiplication/mxm_amp_tiled/kernel.cu
C++AMP/MersenneTwister/box_muller_kernel/kernel.cu
C++AMP/MersenneTwister/rand_MT_kernel/kernel.cu
C++AMP/NBody_Simulation/simple_implementation/kernel.cu
C++AMP/NBody_Simulation/tiling_implementation/kernel.cu
C++AMP/OceanCS/kernel.cu
C++AMP/TransitiveClosure/stage1/kernel.cu
C++AMP/TransitiveClosure/stage2/kernel.cu
C++AMP/TransitiveClosure/stage3/kernel.cu
CUDA20/bitonicsort/kernel.cu
CUDA20/histogram64/histogram64Kernel/kernel.cu
CUDA20/histogram64/mergeHistogram64Kernel/kernel.cu
CUDA20/scan/best/kernel.cu
CUDA20/scan/naive/kernel.cu
CUDA20/scan/workefficient/kernel.cu
CUDA20/scanlarge/inline/kernel.cu
CUDA20/scanlarge/uniformAdd/kernel.cu
CUDA50/0_Simple/asyncAPI/asyncAPI.cu
CUDA50/0_Simple/cppIntegration/kernel.cu
CUDA50/0_Simple/cppIntegration/kernel2.cu
CUDA50/0_Simple/cudaOpenMP/cudaOpenMP.cu
CUDA50/0_Simple/matrixMul/matrixMul.cu
CUDA50/0_Simple/simpleAssert/simpleAssert.cu
CUDA50/0_Simple/simpleAtomicIntrinsics/simpleAtomicIntrinsics.cu
CUDA50/0_Simple/simpleCallback/simpleCallback.cu
CUDA50/0_Simple/simpleCubemapTexture/simpleCubemapTexture.cu
CUDA50/0_Simple/simpleIPC/simpleIPC.cu
CUDA50/0_Simple/simpleLayeredTexture/simpleLayeredTexture.cu
CUDA50/0_Simple/simpleMPI/simpleMPI.cu
CUDA50/0_Simple/simpleMultiCopy/simpleMultiCopy.cu
CUDA50/0_Simple/simpleP2P/simpleP2P.cu
CUDA50/0_Simple/simplePitchLinearTexture/shiftArray.cu
CUDA50/0_Simple/simplePitchLinearTexture/shiftPitchLinear.cu
CUDA50/0_Simple/simpleSeparateCompilation/simpleSeparateCompilation.cu
CUDA50/0_Simple/simpleTexture/simpleTexture.cu
CUDA50/0_Simple/simpleVoteIntrinsics/VoteAllKernel2.cu
CUDA50/0_Simple/simpleVoteIntrinsics/VoteAnyKernel1.cu
CUDA50/0_Simple/simpleVoteIntrinsics/VoteAnyKernel3.cu
CUDA50/0_Simple/simpleZeroCopy/simpleZeroCopy.cu
CUDA50/0_Simple/template/template.cu
CUDA50/0_Simple/template_runtime/template_runtime.cu
CUDA50/0_Simple/vectorAdd/vectorAdd.cu
CUDA50/2_Graphics/marchingCubes/classifyVoxel.cu
CUDA50/2_Graphics/simpleGL/simpleGL.cu
CUDA50/2_Graphics/simpleTexture3D/simpleTexture3D_kernel.cu
CUDA50/3_Imaging/HSOpticalFlow/addKernel.cu
CUDA50/3_Imaging/HSOpticalFlow/derivativesKernel.cu
CUDA50/3_Imaging/HSOpticalFlow/downscaleKernel.cu
CUDA50/3_Imaging/HSOpticalFlow/solverKernel.cu
CUDA50/3_Imaging/HSOpticalFlow/upscaleKernel.cu
CUDA50/3_Imaging/HSOpticalFlow/warpingKernel.cu
CUDA50/3_Imaging/SobelFilter/SobelCopyImage.cu
CUDA50/3_Imaging/SobelFilter/SobelShared.cu
CUDA50/3_Imaging/SobelFilter/SobelTex.cu
CUDA50/3_Imaging/bicubicTexture/d_render.cu
CUDA50/3_Imaging/bicubicTexture/d_renderBicubic.cu
CUDA50/3_Imaging/bicubicTexture/d_renderCatRom.cu
CUDA50/3_Imaging/bicubicTexture/d_renderFastBicubic.cu
CUDA50/3_Imaging/bilateralFilter/bilateralFilter.cu
CUDA50/3_Imaging/boxFilter/d_boxfilter_rgba_x.cu
CUDA50/3_Imaging/boxFilter/d_boxfilter_x_global.cu
CUDA50/3_Imaging/boxFilter/d_boxfilter_x_tex.cu
CUDA50/3_Imaging/boxFilter/d_boxfilter_y_global.cu
CUDA50/3_Imaging/boxFilter/d_boxfilter_y_tex.cu
CUDA50/3_Imaging/convolutionFFT2D/modulateAndNormalize_kernel.cu
CUDA50/3_Imaging/convolutionFFT2D/padDataClampToBorder_kernel.cu
CUDA50/3_Imaging/convolutionFFT2D/padKernel_kernel.cu
CUDA50/3_Imaging/convolutionFFT2D/spPostprocess2D_kernel.cu
CUDA50/3_Imaging/convolutionFFT2D/spPreprocess2D_kernel.cu
CUDA50/3_Imaging/convolutionFFT2D/spProcess2D_kernel.cu
CUDA50/3_Imaging/convolutionSeparable/convolutionColumnsKernel.cu
CUDA50/3_Imaging/convolutionSeparable/convolutionRowsKernel.cu
CUDA50/3_Imaging/convolutionTexture/convolutionColumnsKernel.cu
CUDA50/3_Imaging/convolutionTexture/convolutionRowsKernel.cu
CUDA50/3_Imaging/dct8x8/CUDAkernel1DCT.cu
CUDA50/3_Imaging/dct8x8/CUDAkernel1IDCT.cu
CUDA50/3_Imaging/dct8x8/CUDAkernelQuantizationFloat.cu
CUDA50/3_Imaging/dct8x8/CUDAkernelQuantizationShort.cu
CUDA50/3_Imaging/dwtHaar1D/dwtHaar1D.cu
CUDA50/3_Imaging/histogram/histogram256.cu
CUDA50/3_Imaging/histogram/mergeHistogram256Kernel.cu
CUDA50/3_Imaging/histogram/mergeHistogram64Kernel.cu
CUDA50/3_Imaging/imageDenoising/imageDenoising_copy_kernel.cu
CUDA50/3_Imaging/imageDenoising/imageDenoising_knn_kernel.cu
CUDA50/3_Imaging/imageDenoising/imageDenoising_nlm2_kernel.cu
CUDA50/3_Imaging/imageDenoising/imageDenoising_nlm_kernel.cu
CUDA50/3_Imaging/recursiveGaussian/d_simpleRecursive_rgba.cu
CUDA50/3_Imaging/recursiveGaussian/d_transpose.cu
CUDA50/4_Finance/MonteCarloMultiGPU/MonteCarloOneBlockPerOption.cu
CUDA50/4_Finance/MonteCarloMultiGPU/rngSetupStates.cu
CUDA50/4_Finance/SobolQRNG/sobol.cu
CUDA50/4_Finance/binomialOptions/binomialOptions.cu
CUDA50/4_Finance/quasirandomGenerator/inverseCNDKernel.cu
CUDA50/4_Finance/quasirandomGenerator/quasirandomGeneratorKernel.cu
CUDA50/5_Simulations/fluidsGL/addForces_k.cu
CUDA50/5_Simulations/fluidsGL/advectVelocity_k.cu
CUDA50/5_Simulations/fluidsGL/updateVelocity_k.cu
CUDA50/5_Simulations/oceanFFT/calculateSlopeKernel.cu
CUDA50/5_Simulations/oceanFFT/generateSpectrumKernel.cu
CUDA50/5_Simulations/oceanFFT/updateHeightmapKernel.cu
CUDA50/5_Simulations/particles/calcHashD.cu
CUDA50/6_Advanced/FunctionPointers/SobelCopyImage.cu
CUDA50/6_Advanced/alignedTypes/alignedTypes.cu
CUDA50/6_Advanced/concurrentKernels/sum.cu
CUDA50/6_Advanced/eigenvalues/bisect_kernel_large_onei.cu
CUDA50/6_Advanced/fastWalshTransform/fwtBatch1Kernel.cu
CUDA50/6_Advanced/fastWalshTransform/fwtBatch2Kernel.cu
CUDA50/6_Advanced/fastWalshTransform/modulateKernel.cu
CUDA50/6_Advanced/lineOfSight/computeAngles.cu
CUDA50/6_Advanced/lineOfSight/computeVisibilities.cu
CUDA50/6_Advanced/mergeSort/bitonicMergeElementaryIntervalsKernel.cu
CUDA50/6_Advanced/mergeSort/bitonicSortSharedKernel.cu
CUDA50/6_Advanced/mergeSort/generateSampleRanksKernel.cu
CUDA50/6_Advanced/mergeSort/mergeElementaryIntervalsKernel.cu
CUDA50/6_Advanced/mergeSort/mergeRanksAndIndicesKernel.cu
CUDA50/6_Advanced/reduction/reduce0.cu
CUDA50/6_Advanced/reduction/reduce1.cu
CUDA50/6_Advanced/reduction/reduce2.cu
CUDA50/6_Advanced/reduction/reduce3.cu
CUDA50/6_Advanced/reduction/reduce4.cu
CUDA50/6_Advanced/reduction/reduce5.cu
CUDA50/6_Advanced/reduction/reduce6.cu
CUDA50/6_Advanced/scalarProd/scalarProd.cu
CUDA50/6_Advanced/scalarProd/scalarProdIntraGroup.cu
CUDA50/6_Advanced/scan/scanExclusiveShared.cu
CUDA50/6_Advanced/scan/scanExclusiveShared2.cu
CUDA50/6_Advanced/scan/uniformUpdate.cu
CUDA50/6_Advanced/segmentationTreeThrust/addScalar.cu
CUDA50/6_Advanced/segmentationTreeThrust/calculateEdgesInfo.cu
CUDA50/6_Advanced/segmentationTreeThrust/getRepresentatives.cu
CUDA50/6_Advanced/segmentationTreeThrust/getSuccessors.cu
CUDA50/6_Advanced/segmentationTreeThrust/invalidateLoops.cu
CUDA50/6_Advanced/segmentationTreeThrust/makeNewEdges.cu
CUDA50/6_Advanced/segmentationTreeThrust/markSegments.cu
CUDA50/6_Advanced/shfl_scan/shfl_scan_test.cu
CUDA50/6_Advanced/shfl_scan/uniform_add.cu
CUDA50/6_Advanced/simpleHyperQ/sum.cu
CUDA50/6_Advanced/sortingNetworks/bitonicMergeGlobal.cu
CUDA50/6_Advanced/sortingNetworks/bitonicSortShared.cu
CUDA50/6_Advanced/sortingNetworks/bitonicSortShared1.cu
CUDA50/6_Advanced/sortingNetworks/oddEvenMergeGlobal.cu
CUDA50/6_Advanced/sortingNetworks/oddEvenMergeSortShared.cu
CUDA50/6_Advanced/threadFenceReduction/reduceMultiPass.cu
CUDA50/6_Advanced/transpose/copySharedMem.cu
CUDA50/6_Advanced/transpose/transposeCoalesced.cu
CUDA50/6_Advanced/transpose/transposeCoarseGrained.cu
CUDA50/6_Advanced/transpose/transposeDiagonal.cu
CUDA50/6_Advanced/transpose/transposeFineGrained.cu
CUDA50/6_Advanced/transpose/transposeNaive.cu
CUDA50/6_Advanced/transpose/transposeNoBankConflicts.cu
CUDA50/7_CUDALibraries/MC_SingleAsianOptionP/computeValue.cu
CUDA50/7_CUDALibraries/MC_SingleAsianOptionP/initRNG.cu
gpgpu-sim_ispass2009/AES/aesDecrypt128_kernel/kernel.cu
gpgpu-sim_ispass2009/AES/aesDecrypt256_kernel/kernel.cu
gpgpu-sim_ispass2009/CP/cuda/cuenergy_pre8_coalesce.cu
gpgpu-sim_ispass2009/CP/cuda_base/cuenergy_pre.cu
gpgpu-sim_ispass2009/CP/cuda_short/cuenergy_pre8_coalesce.cu
gpgpu-sim_ispass2009/DG/MaxwellsGPU_RK_Kernel3D/kernel.cu
gpgpu-sim_ispass2009/DG/MaxwellsGPU_VOL_Kernel3D/kernel.cu
gpgpu-sim_ispass2009/DG/partial_get_kernel3d/kernel.cu
gpgpu-sim_ispass2009/LIB/Pathcalc_Portfolio_KernelGPU/kernel.cu
gpgpu-sim_ispass2009/LIB/Pathcalc_Portfolio_KernelGPU2/kernel.cu
gpgpu-sim_ispass2009/LPS/laplace3d_kernel.cu
gpgpu-sim_ispass2009/NN/executeFirstLayer.cu
gpgpu-sim_ispass2009/NN/executeFourthLayer.cu
gpgpu-sim_ispass2009/NN/executeSecondLayer.cu
gpgpu-sim_ispass2009/NN/executeThirdLayer.cu
gpgpu-sim_ispass2009/NQU/nqueen.cu
gpgpu-sim_ispass2009/RAY/color.cu
gpgpu-sim_ispass2009/RAY/d_render.cu
gpgpu-sim_ispass2009/RAY/rayCalc.cu
gpgpu-sim_ispass2009/RAY/rayCast.cu
gpgpu-sim_ispass2009/RAY/rayTrace.cu
gpgpu-sim_ispass2009/WP/generated.cu
parboil/bfs/BFS_kernel/kernel.cl
parboil/cutcp/opencl_cutoff_potential_lattice/kernel.cl
parboil/histo/histo_final/kernel.cl
parboil/histo/histo_intermediates/kernel.cl
parboil/histo/histo_main/kernel.cl
parboil/histo/histo_prescan/kernel.cl
parboil/mri-gridding/binning/kernel.cl
parboil/mri-gridding/gridding/kernel.cl
parboil/mri-gridding/reorder/kernel.cl
parboil/mri-gridding/scan_L1/kernel.cl
parboil/mri-gridding/scan_inter1/kernel.cl
parboil/mri-gridding/scan_inter2/kernel.cl
parboil/mri-gridding/splitRearrange/kernel.cl
parboil/mri-gridding/uniformAdd/kernel.cl
parboil/mri-q/ComputePhiMag/kernel.cl
parboil/sad/larger_sad_calc_16/kernel.cl
parboil/sad/mb_sad_calc/kernel.cl
parboil/sgemm/mysgemmNT/kernel.cl
parboil/spmv/spmv_jds_native/kernel.cl
parboil/stencil/naive/kernel.cl
parboil/tpacf/gen_hists/kernel.cl
polybench/datamining/correlation/kernel0.cl
polybench/datamining/correlation/kernel1.cl
polybench/datamining/correlation/kernel2.cl
polybench/datamining/correlation/kernel3.cl
polybench/datamining/correlation/kernel4.cl
polybench/datamining/correlation/kernel5.cl
polybench/datamining/correlation/kernel6.cl
polybench/datamining/correlation/kernel7.cl
polybench/datamining/correlation/kernel8.cl
polybench/datamining/covariance/kernel0.cl
polybench/datamining/covariance/kernel1.cl
polybench/datamining/covariance/kernel2.cl
polybench/linear-algebra/kernels/2mm/kernel0.cl
polybench/linear-algebra/kernels/3mm/kernel0.cl
polybench/linear-algebra/kernels/3mm/kernel1.cl
polybench/linear-algebra/kernels/3mm/kernel2.cl
polybench/linear-algebra/kernels/3mm/kernel3.cl
polybench/linear-algebra/kernels/atax/kernel0.cl
polybench/linear-algebra/kernels/bicg/kernel0.cl
polybench/linear-algebra/kernels/gemver/kernel0.cl
polybench/linear-algebra/kernels/gemver/kernel1.cl
polybench/linear-algebra/kernels/mvt/kernel0.cl
polybench/linear-algebra/kernels/symm/kernel0.cl
polybench/linear-algebra/kernels/symm/kernel4.cl
polybench/linear-algebra/kernels/trisolv/kernel0.cl
polybench/linear-algebra/kernels/trisolv/kernel1.cl
polybench/linear-algebra/solvers/durbin/kernel2.cl
polybench/linear-algebra/solvers/durbin/kernel8.cl
polybench/linear-algebra/solvers/dynprog/kernel0.cl
polybench/linear-algebra/solvers/dynprog/kernel1.cl
polybench/linear-algebra/solvers/dynprog/kernel2.cl
polybench/linear-algebra/solvers/dynprog/kernel3.cl
polybench/linear-algebra/solvers/gramschmidt/kernel1.cl
polybench/linear-algebra/solvers/gramschmidt/kernel3.cl
polybench/linear-algebra/solvers/gramschmidt/kernel5.cl
polybench/linear-algebra/solvers/gramschmidt/kernel6.cl
polybench/linear-algebra/solvers/lu/kernel0.cl
polybench/stencils/adi/kernel0.cl
polybench/stencils/adi/kernel1.cl
polybench/stencils/adi/kernel2.cl
polybench/stencils/adi/kernel3.cl
polybench/stencils/adi/kernel4.cl
polybench/stencils/adi/kernel5.cl
polybench/stencils/adi/kernel6.cl
polybench/stencils/fdtd-2d/kernel0.cl
polybench/stencils/fdtd-2d/kernel1.cl
polybench/stencils/fdtd-2d/kernel2.cl
polybench/stencils/jacobi-1d-imper/kernel0.cl
polybench/stencils/jacobi-2d-imper/kernel0.cl
rodinia_2.4/b+tree/findK/kernel.cl
rodinia_2.4/b+tree/findRangeK/kernel.cl
rodinia_2.4/backprop/bpnn_adjust_weights/kernel.cl
rodinia_2.4/backprop/bpnn_layerforward/kernel.cl
rodinia_2.4/bfs/BFS_1/kernel.cl
rodinia_2.4/bfs/BFS_2/kernel.cl
rodinia_2.4/cfd/compute_flux/kernel.cl
rodinia_2.4/cfd/compute_step_factor/kernel.cl
rodinia_2.4/cfd/initialize_variables/kernel.cl
rodinia_2.4/cfd/memset/kernel.cl
rodinia_2.4/cfd/time_step/kernel.cl
rodinia_2.4/kmeans/kmeans/kernel.cl
rodinia_2.4/leukocyte/GICOV/kernel.cl
rodinia_2.4/leukocyte/dilate/kernel.cl
rodinia_2.4/lud/lud_diagonal/kernel.cl
rodinia_2.4/lud/lud_perimeter/kernel.cl
rodinia_2.4/particlefilter/find_index_single/kernel.cl
rodinia_2.4/particlefilter/normalize_weights_single/kernel.cl
rodinia_2.4/particlefilter/sum_single/kernel.cl
rodinia_2.4/pathfinder/dynproc/kernel.cl
rodinia_2.4/srad/compress/kernel.cl
rodinia_2.4/srad/extract/kernel.cl
rodinia_2.4/srad/prepare/kernel.cl
rodinia_2.4/srad/reduce/kernel.cl
rodinia_2.4/srad/srad/kernel.cl
rodinia_2.4/srad/srad2/kernel.cl
rodinia_2.4/streamcluster/memset/kernel.cl
rodinia_2.4/streamcluster/pgain/kernel.cl
shoc/bfs/iiit/kernel.cl
shoc/bfs/uiuc_spill/BFS_kernel_SM_block/kernel.cl
shoc/bfs/uiuc_spill/BFS_kernel_multi_block/kernel.cl
shoc/bfs/uiuc_spill/BFS_kernel_one_block/kernel.cl
shoc/bfs/uiuc_spill/Frontier_copy/kernel.cl
shoc/devicememory/readConstantMemoryCoalesced/kernel.cl
shoc/devicememory/readGlobalMemoryCoalesced/kernel.cl
shoc/devicememory/readGlobalMemoryUnit/kernel.cl
shoc/devicememory/readImg/kernel.cl
shoc/devicememory/readInCache/kernel.cl
shoc/devicememory/readLocalMemory/kernel.cl
shoc/devicememory/readRand/kernel.cl
shoc/devicememory/writeGlobalMemoryCoalesced/kernel.cl
shoc/devicememory/writeGlobalMemoryUnit/kernel.cl
shoc/devicememory/writeLocalMemory/kernel.cl
shoc/fft/chk1D_512/kernel.cl
shoc/kernelcompile/scan/kernel.cl
shoc/kernelcompile/triad/kernel.cl
shoc/kernelcompile/uniformadd/kernel.cl
shoc/maxflops/Add1/kernel.cl
shoc/maxflops/Add16/kernel.cl
shoc/maxflops/Add2/kernel.cl
shoc/maxflops/Add4/kernel.cl
shoc/maxflops/Add8/kernel.cl
shoc/maxflops/MAdd1/kernel.cl
shoc/maxflops/MAdd16/kernel.cl
shoc/maxflops/MAdd2/kernel.cl
shoc/maxflops/MAdd4/kernel.cl
shoc/maxflops/MAdd8/kernel.cl
shoc/maxflops/Mul1/kernel.cl
shoc/maxflops/Mul16/kernel.cl
shoc/maxflops/Mul2/kernel.cl
shoc/maxflops/Mul4/kernel.cl
shoc/maxflops/Mul8/kernel.cl
shoc/maxflops/MulMAdd1/kernel.cl
shoc/maxflops/MulMAdd2/kernel.cl
shoc/maxflops/MulMAdd4/kernel.cl
shoc/maxflops/MulMAdd8/kernel.cl
shoc/md/kernel.cl
shoc/queuedelay/four/kernel.cl
shoc/queuedelay/one/kernel.cl
shoc/queuedelay/three/kernel.cl
shoc/queuedelay/two/kernel.cl
shoc/reduction/kernel.cl
shoc/s3d/gr_base/kernel.cl
shoc/s3d/ratt10/kernel.cl
shoc/s3d/ratt2/kernel.cl
shoc/s3d/ratt3/kernel.cl
shoc/s3d/ratt4/kernel.cl
shoc/s3d/ratt5/kernel.cl
shoc/s3d/ratt6/kernel.cl
shoc/s3d/ratt7/kernel.cl
shoc/s3d/ratt8/kernel.cl
shoc/s3d/ratt9/kernel.cl
shoc/s3d/rdwdot/kernel.cl
shoc/s3d/rdwdot10/kernel.cl
shoc/s3d/rdwdot2/kernel.cl
shoc/s3d/rdwdot3/kernel.cl
shoc/s3d/rdwdot6/kernel.cl
shoc/s3d/rdwdot7/kernel.cl
shoc/s3d/rdwdot8/kernel.cl
shoc/s3d/rdwdot9/kernel.cl
shoc/scan/bottom_scan/kernel.cl
shoc/scan/reduce/kernel.cl
shoc/scan/top_scan/kernel.cl
shoc/sort/bottom_scan/kernel.cl
shoc/sort/reduce/kernel.cl
shoc/sort/top_scan/kernel.cl
shoc/spmv/csr_scalar/kernel.cl
shoc/spmv/csr_vector/kernel.cl
shoc/spmv/ellpackr/kernel.cl
shoc/stencil2d/CopyRect/kernel.cl
shoc/stencil2d/StencilKernel/kernel.cl
shoc/triad/kernel.cl
AMD_SDK/AESEncryptDecrypt/kernel1/kernel.cl
AMD_SDK/AESEncryptDecrypt/kernel2/kernel.cl
AMD_SDK/AtomicCounters/kernel1/kernel.cl
AMD_SDK/MatrixMulImage/kernel1/kernel.cl
C++AMP/MatrixMultiplication/mxm_amp_simple/kernel.cu
CUDA50/0_Simple/simpleMultiGPU/simpleMultiGPU.cu
CUDA50/0_Simple/simpleStreams/simpleStreams.cu
CUDA50/2_Graphics/bindlessTexture/bindlessTexture.cu
CUDA50/3_Imaging/dwtHaar1D/initValue.cu
CUDA50/5_Simulations/fluidsGL/advectParticles_k.cu
CUDA50/6_Advanced/sortingNetworks/bitonicMergeShared.cu
CUDA50/7_CUDALibraries/MC_EstimatePiInlineP/computeValue.cu
CUDA50/7_CUDALibraries/MC_EstimatePiInlineP/initRNG.cu

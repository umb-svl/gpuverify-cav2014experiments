#!/usr/bin/env python

import sys
import argparse
import subprocess
from cav14configs import CONFIGS

COMMON_ARGS = [
  "--time-as-csv",
  "--canonical-path-prefix=CAV2014Experiments",
  "--from-file=responsive.txt"
]

def main(arg):
  parser = argparse.ArgumentParser(description="Simple frontend to run whole testsuite using CAV2014 configurations.")
  parser.add_argument("--solver", type=str, required=True, choices=['z3', 'cvc4'], help="Solver to use when analysing kernel")
  parser.add_argument("--config", type=str, required=True, choices=['baseline', 'rr', 'rr+bi', 'rr+bi+pa', 'rr+bi+pa+wd', 'noua', 'noam'], help="Configuration to use when analysing kernels")
  args = parser.parse_args(arg)
  extra_args = CONFIGS[args.config] + [ '--solver=%s' % args.solver ]
  cmds = ["gvtester.py"]
  cmds.append('--gvopt=--silent')
  cmds.append('--csv-file=data/%s-%s.csv' % (args.solver, args.config))
  cmds.extend([ '--gvopt=%s' % x for x in extra_args ])
  cmds.extend(COMMON_ARGS)
  cmds.append('.')
  print ' '.join(cmds)
  subprocess.call(cmds)
  return 0

if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))
